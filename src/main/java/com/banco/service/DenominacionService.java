package com.banco.service;


import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.banco.model.Denominacion;

public interface DenominacionService {

    List<Denominacion> findAll();

    Denominacion findById(Integer id);

    Denominacion save(Denominacion usuarioUc);

    void delete(Integer id);
    
//    boolean exists(Integer id);

//    void deleteAll();
//    
//    Page<Denominacion> search(String query, Pageable pageable);
    
}

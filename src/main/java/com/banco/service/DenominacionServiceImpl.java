package com.banco.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.banco.model.Denominacion;
import com.banco.repository.DenominacionRepository;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service
public class DenominacionServiceImpl implements DenominacionService{

    private final Logger log = LoggerFactory.getLogger(DenominacionServiceImpl.class);

    @Autowired
    private DenominacionRepository repository;

    @Override
    @Transactional(readOnly = true)
    public List<Denominacion> findAll() {
        log.debug("Request to get all Denominacion");
        return repository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Denominacion findById(Integer id) {
        log.debug("Request to get Denominacion : {}", id);
        return repository.findAllById(id);
    }

    @Override
    public Denominacion save(Denominacion denominacion) {
        return repository.save(denominacion);
    }

    @Override
    public void delete(Integer id) {
        log.debug("Request to delete Denominacion : {}", id);
        repository.deleteById(id);
    }

//    @Override
//    public void deleteAll() {
//        log.debug("Request to delete All Denominacion");
//        repository.deleteAll();
//    }
//    
//    @Override
//    @Transactional(readOnly = true)
//    public Page<Denominacion> search(String query, Pageable pageable) {
//        log.debug("Request to search for a page of Denominacion");
//        return repository.findAll(pageable);
//    }
//
//	@Override
//	public boolean exists(Integer id) {
//		// TODO Auto-generated method stub
//		return false;
//	}

}

package com.banco.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "denominacion")
@Access(AccessType.FIELD)
public class Denominacion extends ParentEntity {

	private static final long serialVersionUID = 6946553024592095559L;
    
    @Column(name = "descripcion")
	private String descripcion;
    
    @Column(name = "valor")
	private Integer valor;

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Integer getValor() {
		return valor;
	}

	public void setValor(Integer valor) {
		this.valor = valor;
	}
        

}

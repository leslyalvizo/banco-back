package com.banco.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.banco.model.Denominacion;
import com.banco.service.DenominacionService;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.validation.Valid;
import java.net.URISyntaxException;

@RestController
public class DenominacionResource {

    private static final Logger log = LoggerFactory.getLogger(DenominacionResource.class);

    @Autowired
    private DenominacionService service;
    
	@RequestMapping(value = "/denominacion/create", method = RequestMethod.POST,
            consumes = "application/json;charset=UTF-8", produces = "application/json;charset=UTF-8")
    public ResponseEntity<Denominacion> createDenominacion(@Valid @RequestBody Denominacion denominacion) throws URISyntaxException {
        log.debug("REST request to save Denominacion : {}", denominacion);
        Denominacion result = service.save(denominacion);
        return new ResponseEntity<Denominacion>(result, HttpStatus.OK);
    }

	@RequestMapping(value = "/denominacion/update", method = RequestMethod.PUT,
            consumes = "application/json;charset=UTF-8", produces = "application/json;charset=UTF-8")
    public ResponseEntity<Denominacion> updateDenominacion(@Valid @RequestBody Denominacion denominacion) throws URISyntaxException {
        log.debug("REST request to update Denominacion : {}", denominacion);
        Denominacion result = service.save(denominacion);
        return new ResponseEntity<Denominacion>(result, HttpStatus.OK);
    }
    
	@RequestMapping(value = "/denominacion/getall", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public ResponseEntity<List<Denominacion>> getAllDenominacion() {
        log.debug("REST request to get a page of Denominacion");
        List<Denominacion> list = service.findAll();
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

	@RequestMapping(value = "/denominacion/get/{id}", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public ResponseEntity<Denominacion> getDenominacion(@PathVariable Integer id) {
        log.debug("REST request to get Denominacion : {}", id);
        Denominacion denominacion = service.findById(id);
        return new ResponseEntity<Denominacion>(denominacion, HttpStatus.OK);
    }

	@RequestMapping(value = "/denominacion/delete/{id}", method = RequestMethod.DELETE, produces = "application/json;charset=UTF-8")
    public ResponseEntity<Void> deleteDenominacion(@PathVariable Integer id) {
        log.debug("REST request to delete Denominacion : {}", id);
        service.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}

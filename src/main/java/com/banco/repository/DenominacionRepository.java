package com.banco.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.banco.model.Denominacion;

@Repository
public interface DenominacionRepository extends JpaRepository<Denominacion, Integer> {
	
	@SuppressWarnings("unchecked")
	Denominacion save(Denominacion denominacion);

	Denominacion findAllById(Integer id);

	void deleteById(Integer id);
	
	List<Denominacion> findAll();
	
}
